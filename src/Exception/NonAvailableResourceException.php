<?php                                      
                                                     
/* This file is part of Portail Recherche Data Gouv. */

/* Portail Recherche Data Gouv is free software: you can redistribute it and/or modify it 
under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the license,
or (at your option) any later version.

Portail Recherche Data Gouv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY ; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. see the GNU General Public License for more details.

you should have received a copy of the GNU General Public License 
along with Portail Recherche Data Gouv. if not, see https://www.gnu.org/licenses/. */

/* Copyright (C) 2021 INRAE */

namespace Dipso\LockeditBundle\Exception;

use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Context\ExceptionContext;
use EasyCorp\Bundle\EasyAdminBundle\Exception\BaseException;

class NonAvailableResourceException extends BaseException
{
    public function __construct(AdminContext $context)
    {
        $parameters = [
            'resource' => null === $context->getEntity() ? null : $context->getEntity()->getName()." ".$context->getEntity()->getPrimaryKeyValueAsString(),
        ];

        $exceptionContext = new ExceptionContext(
            'exception.already_used',
            sprintf('You can\'t edit "%s". Resource already used', $parameters['resource']),
            $parameters,
            403
        );

        parent::__construct($exceptionContext);
    }
}
