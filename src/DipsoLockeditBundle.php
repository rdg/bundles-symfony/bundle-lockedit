<?php                                      
                                                     
/* This file is part of Portail Recherche Data Gouv. */

/* Portail Recherche Data Gouv is free software: you can redistribute it and/or modify it 
under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the license,
or (at your option) any later version.

Portail Recherche Data Gouv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY ; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. see the GNU General Public License for more details.

you should have received a copy of the GNU General Public License 
along with Portail Recherche Data Gouv. if not, see https://www.gnu.org/licenses/. */

/* Copyright (C) 2021 INRAE */

namespace Dipso\LockeditBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class DipsoLockeditBundle extends Bundle {}
