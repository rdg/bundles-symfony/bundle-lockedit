<?php                                      
                                                     
/* This file is part of Portail Recherche Data Gouv. */

/* Portail Recherche Data Gouv is free software: you can redistribute it and/or modify it 
under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the license,
or (at your option) any later version.

Portail Recherche Data Gouv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY ; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. see the GNU General Public License for more details.

you should have received a copy of the GNU General Public License 
along with Portail Recherche Data Gouv. if not, see https://www.gnu.org/licenses/. */

/* Copyright (C) 2021 INRAE */

namespace Dipso\LockeditBundle\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LockRepository::class)
 * @ORM\Table(name="`lock`")
 */
class Lock
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $resource;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $timestamp;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?string
    {
        return $this->user;
    }

    public function setUser(string $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getResource(): ?string
    {
        return $this->resource;
    }

    public function setResource(string $resource): self
    {
        $this->resource = $resource;

        return $this;
    }

    public function getTimestamp(): ?DateTimeInterface
    {
        return $this->timestamp;
    }

    public function setTimestamp(DateTimeInterface $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }
}
